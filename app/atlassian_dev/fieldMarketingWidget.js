function createFieldMarketingEvent(issueKey){
  var text = "<div class='container-background row'>";

  text += "<div class='col-lg-5' style='padding-top: 20px;float:right;'>";
  text += "<h3  >";
  text += "<span id='status' class='label label-info' style='margin-bottom:5px;display: inline-block;'></span>";
  text += "</h3>";

  text += "</div>";

  text += "<div class='col-lg-7' style='padding: 20px;'>";
  text += "<h3  >";
  text += "<span id='typeOfEvent' class='label label-primary' style='display: inline-block;'></span>";
  text += "</h3>";
  text += "<h2 style='margin:10px 0;'  ><a class='' href='https://extranet.atlassian.com/jira/browse/"+issueKey+"'><span id='title' style='display:inline-block'></span></a><span id='duedate' class='small' style='display:inline-block'> // </span></h2>";
  text += "<h6 >";
  text += "<strong >Approver: <span id='approver' class='label label-info' style='padding-left:0;'><img id='approverPic' src='' style='margin-top:-2px;' /> </span> </strong>";
  text += "</h6>";
  text += "<h6 >";
  text += "<strong >Assignee: <span id='assignee' class='label label-info' style='padding-left:0;'><img id='assigneePic' src='' style='margin-top:-2px;' />  </span> </strong>";
  text += "</h6>   ";
  text += "<span class=''><i class='fa fa-link'></i> </span>";
  text += "<strong>Event Link: </strong>";
  text += "<a id='eventLink' target='_blank'>";
  text += "</a>";
  text += "<br>";
  text += "<address >";
  text += "<span class=''><i class='fa fa-building'></i> </span> ";
  text += "<strong>Address: <a id='address' target='_blank' href='' ></a></strong><br> ";
  text += "</address>";
  text += "<div id=''>";
  text += "<span class=''><i class='fa fa-user'></i> </span>";
  text += "<strong>Speakers: </strong>";
  text += "<span id='speakers' class=''></span>    ";
  text += "</div>";
  text += "<div id=''>";
  text += "<span class=''><i class='fa fa-user'></i> </span>";
  text += "<strong>Experts: </strong>";
  text += "<span id='experts' class='aui-lozenge aui-lozenge-current'></span>      ";
  text += "</div>";
    // text += "<span class=''><i class='fa fa-link'></i></span>";
    // text += "<strong>EAC Link: </strong>";
    // text += "<a id='eacLink'>";
    // text += "</a>";
    text += "</div>";
    
    text += "<div class='col-lg-5' style='padding: 0 20px 20px 20px;margin-bottom: 30px;'>";

    text += "<!--  <img src='https://extranet.atlassian.com/download/attachments/2248132358/Screen%20Shot%202014-06-28%20at%205.51.34%20PM.png'> -->";
   // text += "<h3  >";
   // text += "<span id='status' class='label label-info' style='margin-bottom:5px;display: inline-block;'></span>";
   // text += "</h3>";
   text += "<h5 >";
   text += "<strong ><i class='fa  fa-globe'></i> Country: <span id='country' class=''></span> </strong>";
   text += "</h5>";
   text += "<h5 >";
   text += "<strong ><i class='fa   fa-location-arrow'></i> Region: <span id='region' class=''></span> </strong>";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<strong><i class='fa   fa-crosshairs'></i> Market: </strong>";
   text += "<span id='market'></span>   ";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<strong><i class='fa   fa-file-powerpoint-o'></i> Products: </strong>";
   text += "<span id='products' class=''></span> ";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<span class=''><i class='fa fa-users'></i> </span>";
   text += "<strong>Total Attendance: </strong>";
   text += "<span id='attendance' class='aui-lozenge aui-lozenge-success'></span>   ";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<span class=''><i class='fa  fa-envelope-square'></i> </span>";
   text += "<strong>Leads: </strong>";
   text += "<span id='leads' class='aui-lozenge aui-lozenge-success'></span>    ";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<span class=''><i class='fa fa-users'></i> </span>";
   text += "<strong>Registrations: </strong>";
   text += "<span id='registrations' class='aui-lozenge aui-lozenge-success'></span>   ";
   text += "</h5>";
   text += "<h5 id=''>";
   text += "<span class=''><i class='fa fa-users'></i> </span>";
   text += "<strong>Speaker Audience: </strong>";
   text += "<span id='speakerAudience' class='aui-lozenge aui-lozenge-success'></span>   ";
   text += "</h5>";
   text += "</div>";


   

   text += "<div class='col-lg-12' style='padding: 20px;position:absolute;bottom:0px;right:0; text-align:right;'>";
   text += "<div>";
   text += "<a id='JIRAPOP' class='btn btn-success'  style='color:white' href='#'><i class='fa fa-cog'></i>POP</a> "; 
   text += " <a class='btn btn-primary'  style='color:white' href='https://extranet.atlassian.com/jira/browse/"+issueKey+"'><i class='fa fa-cog'></i> Edit / Add issues</a>"; 
   text += "</div>";
   text += "</div>";

    text += "</div>";

   text += "<div id='galleria' class='col-lg-12 container-background'>";
   text += "";
   text += "</div>";


 text += "<div id='backgroundJIRA' style='background-color:rgba(0,0,0,0.4);display:none;position:fixed;right:0;width:100%;  top: 0;height:100%;'></div>";

    // text += "<iframe id='mainJIRA' style='display:none;position:fixed;right:0;min-width:1000px;width:70%;  top: 5%;height:100%;' src='https://extranet.atlassian.com/jira/browse/"+issueKey+"?jql=project=FM%20ORDER%20BY%20due%20DESC'></iframe>";

    text += "<iframe id='mainJIRA' style='display:none;position:fixed;right:0;max-width:1000px;width:70%;  top: 5%;height:100%;' src='https://extranet.atlassian.com/jira/browse/"+issueKey+"'></iframe>";


    $("#mainDiv").append(text);



    var myEl = document.getElementById('JIRAPOP');
    myEl.addEventListener('click', function() {
      $("#mainJIRA").toggle();
      $("#backgroundJIRA").toggle();
    }, false);

var myEl = document.getElementById('backgroundJIRA');
    myEl.addEventListener('click', function() {
      $("#mainJIRA").toggle();
      $("#backgroundJIRA").toggle();
    }, false);






   //var myNewURL= 'https://extranet.atlassian.com/jira/rest/api/2/search?fields=attachment&jql=key%20%3D%20'+issueKey;
   var myNewURL= 'https://extranet.atlassian.com/jira/rest/api/latest/issue/'+issueKey;

   AJS.$.get(myNewURL, function(data){

    AJS.$("#title").append(data.fields.customfield_13081);
    AJS.$("#address").append(data.fields.customfield_18585);
    $("#address").attr( "href" , "https://www.google.com/maps/place/"+data.fields.customfield_18585);

    AJS.$("#eventLink").append(data.fields.customfield_18388);
    $("#eventLink").attr( "href" , data.fields.customfield_18388);

    AJS.$("#eacLink").append(data.fields.customfield_18389);
    $("#eacLink").attr( "href" , data.fields.customfield_18389);


    AJS.$("#attendance").append(data.fields.customfield_18380);
    AJS.$("#leads").append(data.fields.customfield_18381);
    AJS.$("#registrations").append(data.fields.customfield_18584);
    AJS.$("#speakerAudience").append(data.fields.customfield_18387);

    AJS.$("#duedate").append(moment(data.fields.duedate).format('dddd MMMM Do YYYY'));


    AJS.$("#experts").append(data.fields.customfield_18393);


    if( data.fields.customfield_18391 != null){
      AJS.$("#approver").append(data.fields.customfield_18391.displayName);
            //AJS.$("#approverPic").href(data.fields.customfield_18391.avatarUrls.16x16);
            //$("#approverPic").attr( "src" , data.fields.customfield_18391.avatarUrls.16x16);
            $("#approverPic").attr( "src" , data.fields.customfield_18391.avatarUrls['16x16']);


          }

          if(data.fields.assignee != null){
            AJS.$("#assignee").append(data.fields.assignee.displayName);
            //  AJS.$("#assigneePic").attr( "src" , data.fields.assignee.avatarUrls.16x16);
            //console.log(data.fields.assignee.avatarUrls['16x16']);
            AJS.$("#assigneePic").attr( "src" , data.fields.assignee.avatarUrls['16x16']);

          }
          if(data.fields.customfield_18386 != null){
            data.fields.customfield_18386.forEach(function(entry){
              AJS.$("#market").append("<span class='aui-lozenge  aui-lozenge-complete'>"+entry.value+"</span> ");
            });
          }
          if(data.fields.customfield_18385 != null){
            data.fields.customfield_18385.forEach(function(entry){
              AJS.$("#products").append("<span class='aui-lozenge  aui-lozenge-complete'>"+entry.value+"</span> ");
            });
          } 
          if(data.fields.customfield_18581 != null){
            data.fields.customfield_18581.forEach(function(entry){
              AJS.$("#speakers").append("<span class='aui-lozenge aui-lozenge-complete'>"+entry.displayName+"</span> ");
            });
          } 
          if(data.fields.customfield_18382 != null){
            data.fields.customfield_18382.forEach(function(entry){
              AJS.$("#country").append("<span class='aui-lozenge aui-lozenge-subtle aui-lozenge-complete'>"+entry+"</span> ");
            });
          } 
          if(data.fields.customfield_18383 != null){
            data.fields.customfield_18383.forEach(function(entry){
              AJS.$("#region").append("<span class='aui-lozenge aui-lozenge-subtle aui-lozenge-complete'>"+entry+"</span> ");
            });
          } 

          if( data.fields.status != null){
            //alert(data.fields.status['statusCategory']['colorName']);
            AJS.$("#status").append(data.fields.status['name']);
            if(data.fields.status['statusCategory']['colorName'] == 'blue-gray'){
                //alert(data.fields.status['statusCategory']['colorName']);
                AJS.$("#status").css( "background-color", '#4a6785', 'color', 'white' );
              }
              if(data.fields.status['statusCategory']['colorName'] == 'yellow'){
                //alert(data.fields.status['statusCategory']['colorName']);
                AJS.$("#status").css( "background-color", '#ffd351' );
                AJS.$("#status").css('color', '#594300' );
              }
              if(data.fields.status['statusCategory']['colorName'] == 'green'){
                //alert(data.fields.status['statusCategory']['colorName']);
                AJS.$("#status").css( "background-color", '#14892c', 'color', 'white' );
              }


            }

            if( data.fields.customfield_18583 != null){
              //alert(data.fields.status['statusCategory']['colorName']);
              AJS.$("#typeOfEvent").append(data.fields.customfield_18583['value']);
              if(data.fields.customfield_18583['value'] == 'Product On Tour'){
                //alert(data.fields.status['statusCategory']['colorName']);
                AJS.$("#typeOfEvent").css( "background-color", 'rgb(115, 6, 177)', 'color', 'white' );
              }



            }
            if(data.fields.attachment != null){
              console.log(data.fields.attachment);
              data.fields.attachment.forEach(function(entry){
                if(( entry['mimeType'] == "image/jpeg") || ( entry['mimeType'] == "image/gif") || ( entry['mimeType'] == "image/png")){
                  AJS.$("#galleria").append("<img class='confluence-embedded-image confluence-thumbnail' height=50px src="+entry['thumbnail']+" data-image-src='"+entry['thumbnail']+"' > ");  

                }
                
              });
            } 



          });


}