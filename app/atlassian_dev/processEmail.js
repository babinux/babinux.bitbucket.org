
function processEmails(Array, myDiv){
	var i = 0;
	var nbFiles = 0;

	var totalReach = 0;
	var uniqueLeads = [];
	var corporateLeads = [];
	var eduLeads = [];
	var emeaLeads = [];
	var apacLeads = [];
	var americasLeads = [];
	var f_beneluxLeads = [];
	var totalByEvent = 0;
	var allMyCvs =  [];
	var allMyCvs2 =  [];

	Array.forEach(function(entry) {

		AJS.$.get(entry, function(data){
			var emailsEventLine = '';				
			var j = 0;
			var k = 0;
			var table = CSVToArray(data);

			table.forEach(function(line) {
				k = 0;
				emailsEventLine += "<tr>";
				emailsEventLine += "<td>"+i+"●</td>";
				emailsEventLine += "<td>"+j+"●</td>";

				//console.log(table.length+" "+j);
				if(j == table.length-1){
					var lengthTable = table.length-1;
					totalByEvent += lengthTable;
					var pastEvent = "<li class='event_in_list' style='float:left;clear:both;background-color: black;color: white;padding:3px;margin:2px;'>"+table[1][2]+" | <span style='background-color:white;color:black;border-radius:150px;padding:2px;'>"+lengthTable+"</span></li>";
					AJS.$("#list_event").append(pastEvent);
					allMyCvs.push("\n"+data);
					allMyCvs2 += data;						
				}


				if(j == 0){
					j++;
					nbFiles++;
					line.forEach(function(item) {
						k++;
						emailsEventLine += "<td class='headerRow'>"+item+"</td>";
						if (k == 8){
							totalReach += 1;
						}
					});
				}else{
					j++;
					i++;
					

					line.forEach(function(item) {
						k++;

						if (k == 5){
							var emea = /EMEA/.test(item);
							var apac = /APAC/.test(item);
							var americas = /Americas/.test(item);
							var f_benelux = /F+BENELUX/.test(item);
							var benelux = /BENELUX/.test(item);

							// console.log(item);

							// console.log(emea);
							// console.log(apac);
							if ((emea)){
								emeaLeads.push(item);
							}
							if ((apac)){
								apacLeads.push(item);
							}
							if ((americas)){
								americasLeads.push(item);
							}
							if ((f_benelux || benelux)){
								f_beneluxLeads.push(item);
							}
						}
						if (k == 8){

							uniqueLeads.push(item);

							var yahoo = /@yahoo.com\s*$/.test(item)
							var gmail = /@gmail.com\s*$/.test(item)
							var hotmail = /@hotmail\s*$/.test(item)
							var edu = /@.*.edu\s*$/.test(item)

							if (!(yahoo || gmail || hotmail)){
								corporateLeads.push(item);
									//console.log(yahoo);
								}
								if (edu){
									eduLeads.push(item);
									//console.log(edu);
								}

							}
							emailsEventLine += "<td>"+item+"</td>";

							if((line.length == k)&&(Array.length == nbFiles)&&(table.length == j)){
								
								AJS.$(".nb_events", myDiv ).append(Array.length);
								AJS.$(".nb_leads" , myDiv).append(uniqueLeads.length - Array.length);

								var realUniqueLeads = jQuery.unique( uniqueLeads );
								AJS.$(".nb_unique_leads", myDiv).append(realUniqueLeads.length - Array.length);
								AJS.$(".nb_corporate_leads", myDiv).append(corporateLeads.length - Array.length);
								AJS.$(".nb_edu_leads", myDiv).append(eduLeads.length);
								AJS.$(".nb_emea_leads", myDiv).append(emeaLeads.length);
								AJS.$(".nb_apac_leads", myDiv).append(apacLeads.length);

								AJS.$(".nb_f_benelux_leads", myDiv).append(f_beneluxLeads.length);
								AJS.$(".nb_americas_leads", myDiv).append(americasLeads.length);

								var pastEvent = "<li class='event_in_list' style='float:left;clear:both;background-color: black;color: white;padding:3px;margin:2px;'>total number of rows: "+totalByEvent+"</li>";
								AJS.$(".list_event", myDiv).append(pastEvent);
								

								var aFileParts = [allMyCvs];
								var oMyBlob = new Blob(aFileParts, {type : 'text/csv'}); 
								var url = URL.createObjectURL(oMyBlob);
								AJS.$(".downloadFile", myDiv).append("<a href="+url+" download='csvFromConfluence.csv'>Download combined CSV of all the leads</a>");

							}
						});
}
emailsEventLine += "</tr>";
});
AJS.$("#csv_outlet").append(emailsEventLine);
});
});
return uniqueLeads;
}


function CSVToArray( strData, strDelimiter ){
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");
        var objPattern = new RegExp(
        	(
                // Delimiters.
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

                // Quoted fields.
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

                // Standard fields.
                "([^\"\\" + strDelimiter + "\\r\\n]*))"
        	),
        	"gi"
        	);
        var arrData = [[]];
        var arrMatches = null;
        while (arrMatches = objPattern.exec( strData )){
        	var strMatchedDelimiter = arrMatches[ 1 ];
        	if (
        		strMatchedDelimiter.length &&
        		strMatchedDelimiter !== strDelimiter
        		){
        		arrData.push( [] );

        }

        var strMatchedValue;
        if (arrMatches[ 2 ]){
        	strMatchedValue = arrMatches[ 2 ].replace(
        		new RegExp( "\"\"", "g" ),
        		"\""
        		);

        } else {
        	strMatchedValue = arrMatches[ 3 ];

        }
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }
    return( arrData );
}
